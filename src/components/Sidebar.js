import React, { Component } from 'react';
import '../css/App.css';

export default class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.props.closeSideBar()
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  render() {
    let { open, styles, title, content } = this.props

    if (!open) {
      return null
    }

    return (
      <div className="sidebar" style={styles} ref={this.setWrapperRef} >
        <div className="heading border-bottom text-center">{title}</div>
        <div className="sidebar-content">
          {content}
        </div>
      </div>
    )
  }
}