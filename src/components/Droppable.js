import React, { Component } from 'react';
import '../css/App.css';
import { renderElement } from '../utils/helper'
import * as enums from '../utils/enums'
import Sidebar from "./Sidebar";


export default class AppDragDropDemo extends Component {
  constructor (props) {
    super(props)
    let items = JSON.parse(localStorage.getItem('builder')) || []
    this.state = {
      items,
      sidebarOpen: false,
      selectedElement: {}
    }

    this.openSideBar = this.openSideBar.bind(this)
    this.closeSideBar = this.closeSideBar.bind(this)
    this.deleteItem = this.deleteItem.bind(this)
  }
    onDragStart = (ev, id) => {
        ev.dataTransfer.setData("id", id);
    }

    onDragOver = (ev) => {
        ev.preventDefault();
    }

    onDrop = (ev) => {
      let type = ev.dataTransfer.getData("type")
      let id = parseInt(ev.dataTransfer.getData("id"))
      let items = this.state.items
      let x = ev.clientX, y = ev.clientY
      let item;
      let isNewItem = !id
      let lastItem = items[items.length - 1]

      if (isNewItem) {
        item = {
          type,
          x,
          y,
          id: lastItem ? lastItem.id + 1: 1,
          position: 'absolute',
          label: this.shouldShowLabel(type) && '',
          placeholder: this.shouldShowPlaceholder(type) && type,
          content: this.shouldShowContent(type) && type,
        }
      } else {
        item = items.find(it => it.id === id)
        Object.assign(item, { x, y })
      }

      this.updateItems(item, isNewItem)
      this.openSideBar(item)
    }

    shouldShowContent(type) {
      return type === enums.ELEMENT_TYPES.TEXT_VIEW
    }

    shouldShowPlaceholder(type) {
      return [enums.ELEMENT_TYPES.TEXT_BOX, enums.ELEMENT_TYPES.PASSWORD,
        enums.ELEMENT_TYPES.EMAIL, enums.ELEMENT_TYPES.SELECT].includes(type)
    }

    shouldShowLabel(type) {
      return this.shouldShowPlaceholder(type) || type === enums.ELEMENT_TYPES.BUTTON
    }

    getXYOffset(element) {
      switch (element.type) {
        case enums.ELEMENT_TYPES.TEXT_BOX:
          return { xOffset: 165, yOffset: 31 }
        case enums.ELEMENT_TYPES.BUTTON:
          return { xOffset: 90, yOffset: 20 }
        case enums.ELEMENT_TYPES.TEXT_VIEW:
          return { xOffset: 78, yOffset: 24 }
        case enums.ELEMENT_TYPES.SELECT:
          return { xOffset: 87, yOffset: 23 }
        case enums.ELEMENT_TYPES.EMAIL:
          return { xOffset: 165, yOffset: 31 }
        case enums.ELEMENT_TYPES.PASSWORD:
          return { xOffset: 165, yOffset: 31 }
      }
    }

    getStyle(droppedElement, i) {
      let { xOffset, yOffset } = this.getXYOffset(droppedElement)
      let draggableContainer = document.getElementById("draggable-container")
      let appHeader = document.getElementById("app-header")
        let drawerWidth = draggableContainer ? draggableContainer.offsetWidth : 0
      let headerHeight = appHeader ? appHeader.offsetHeight : 73

      let top = droppedElement.y - yOffset / 2 - headerHeight
      let left = droppedElement.x - xOffset / 2 - drawerWidth

      return { top, left, zIndex: i }
    }

    renderElements() {
    let { selectedElement, items } = this.state
      return items.map ((t, i) => {
        let isSelected = selectedElement.id === t.id
        return (
          <div key={i}
               onDragStart = {(e) => this.onDragStart(e, t.id)}
               onClick={this.openSideBar.bind(this, t)}
               draggable
               id={i}
               className={`dropped-element ${isSelected && 'active'} ${t.position}`}
               style = {this.getStyle(t, i)} >
            {renderElement(t)}
          </div>
        )
      })
    }

    openSideBar(t) {
      this.setState({ sidebarOpen: true, selectedElement: t })
    }


    closeSideBar() {
      this.setState({ sidebarOpen: false, selectedElement: {} })
    }

    renderSidebarContent() {
      let { selectedElement } = this.state
      let type = selectedElement.type
      return (
        <div>
          <form>
            <div className = "form-group">
              <label className="font-weight-bold">Type: </label>
              <span className="text-capitalize ml-2 font-weight-bold">{selectedElement.type}</span>
            </div>
            <div className = "form-group">
              <div className="font-weight-bold">Position</div>
              <label className="radio-inline mr2">
                <input
                  type="radio"
                  value="absolute"
                  checked={this.isChecked("absolute")}
                  onChange={this.handleChange.bind(this, "position")} />
                <span className="ml-2">Absolute</span>
              </label>
              <label className="radio-inline ml-2">
                <input
                  type="radio"
                  value="static"
                  checked={this.isChecked("static")}
                  onChange={this.handleChange.bind(this, "position")} />
                <span className="ml-2">Static</span>
              </label>
            </div>
            {this.shouldShowLabel(type) &&
              <div className = "form-group">
                <label className="font-weight-bold" htmlFor="label">Label</label>
                <input
                  className="form-control form-control-sm"
                  placeholder="Edit Label"
                  id="label"
                  type="text"
                  name="label"
                  value={selectedElement.label}
                  onChange={this.handleChange.bind(this, "label")} />
              </div>
            }
            {this.shouldShowPlaceholder(type) &&
              <div className = "form-group">
                <label className="font-weight-bold" htmlFor="placeholder">Placeholder</label>
                <input
                  className="form-control form-control-sm"
                  placeholder="Edit Placeholder"
                  id="placeholder"
                  type="text"
                  name="placeholder"
                  value={selectedElement.placeholder}
                  onChange={this.handleChange.bind(this, "placeholder")} />
              </div>
            }
            {this.shouldShowContent(type) &&
              <div className = "form-group">
                <label className="font-weight-bold" htmlFor="content">Content</label>
                <textarea
                  className="form-control form-control-sm"
                  placeholder="Edit Content"
                  id="content"
                  type="text"
                  name="content"
                  value={selectedElement.content}
                  onChange={this.handleChange.bind(this, "content")} />
              </div>
            }

          </form>
          <div className = "form-group d-flex justify-content-center">
            <button className="btn btn-danger" onClick={this.deleteItem}>Delete</button>
          </div>
        </div>
      )
    }

    deleteItem() {
      this.updateItems()
      this.closeSideBar()
    }

    isChecked(value) {
      return this.state.selectedElement.position === value
    }

    handleChange(property, e) {
      let { selectedElement } = this.state
      Object.assign(selectedElement, { [property]: e.target.value })
      this.updateItems(selectedElement)
    }

    updateItems(item, isNewItem) {
      let { items, selectedElement } = this.state
      let isDelete = !item
      if (isDelete) {
        item = selectedElement
      }

      let index = items.findIndex(i => i.id === item.id)
      if (isDelete) {
        items.splice(index, 1)
      } else if (item && isNewItem) {
        items.push(item)
      } else {
        items.splice(index, 1, item)
      }

      this.setState({ items })
      this.saveState()
    }

    saveState() {
      localStorage.setItem('builder', JSON.stringify(this.state.items))
    }

    render() {
      let { selectedElement, sidebarOpen } = this.state

      return (
          <div className="droppable-container"
            onDragOver={(e)=>this.onDragOver(e)}
            onDrop={(e)=>this.onDrop(e)}>
              <div className="droppable">
                {this.renderElements()}
                <Sidebar
                  title="PROPERTIES"
                  content={this.renderSidebarContent()}
                  open={sidebarOpen}
                  styles={{ background: "#f8f9fa", width: "250px" }}
                  element={selectedElement}
                  closeSideBar={this.closeSideBar}
                />
              </div>
          </div>
      );
  }
}
