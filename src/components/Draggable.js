import React, { Component } from 'react';
import '../css/App.css';
import * as enums from '../utils/enums'
import { renderElement } from '../utils/helper'

export default class Draggable extends Component {
    state = {
        items: [
            { type: enums.ELEMENT_TYPES.TEXT_BOX },
            { type: enums.ELEMENT_TYPES.EMAIL },
            { type: enums.ELEMENT_TYPES.PASSWORD },
            { type: enums.ELEMENT_TYPES.SELECT },
            { type: enums.ELEMENT_TYPES.BUTTON },
            { type: enums.ELEMENT_TYPES.TEXT_VIEW }
          ]
    }

    onDragStart = (ev, t) => {
        ev.dataTransfer.setData("type", t.type);
    }

    onDragOver = (ev) => {
        ev.preventDefault();
    }

    renderElements() {
      return this.state.items.map ((t) => {
        return (
          <div key={t.type}
               onDragStart = {(e) => this.onDragStart(e, t)}
               draggable
               className="clearfix list-group-item list-group-item-action bg-light border-bottom draggable-element" >
            {renderElement(t)}
          </div>
        );
      });
    }

    render() {

      return (
        <div className="draggable-container" id="draggable-container">
            <div onDragOver={(e)=>this.onDragOver(e)}>

                {this.renderElements()}
            </div>
        </div>
      );
    }
}
