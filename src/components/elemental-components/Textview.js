import React from 'react';

export default function Textview(props){
  let { content } = props.element
  content = content && content.length > 0 ? content : 'Text View'
  return <div className="text-justify text-wrap">{content}</div>
}