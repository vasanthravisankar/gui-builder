import React from 'react';

export default function Selectbox(props){
  let element = props.element
  let { id, label, placeholder, type } = element
  placeholder = placeholder ? placeholder : type
  return (
    <div className={`${id ? `form-group clearfix` : ''}`} style={{width: '193px'}}>
      {label && label.length > 0  && <label className="float-left font-weight-bold" htmlFor={id}>{label}</label>}
      <select id={id} className="form-control" placeholder={placeholder} ><option>Option 1</option></select>
    </div>
  )
}