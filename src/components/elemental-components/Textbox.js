import React from 'react';

export default function Textbox(props){
  let element = props.element
  let { id, label, placeholder, type } = element
  placeholder = placeholder ? placeholder : type
  return (
    <div className={`${id ? `form-group clearfix` : ''}`}>
      {label && label.length > 0  && <label className="float-left font-weight-bold" htmlFor={id}>{label}</label>}
        <input id={id} className="form-control" type="text" placeholder={placeholder} />
    </div>
  )
}