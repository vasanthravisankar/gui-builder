import React from 'react';

export default function Button(props){
  let { label } = props.element
  label = label && label.length > 0 ? label : 'BUTTON'
  return <button className="btn btn-primary">{label}</button>
}