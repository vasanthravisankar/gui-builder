import React from 'react';
import * as enums from '../utils/enums'
import Button from '../components/elemental-components/Button'
import Textbox from '../components/elemental-components/Textbox'
import Textview from '../components/elemental-components/Textview'
import Selectbox from '../components/elemental-components/Selectbox'
import Email from '../components/elemental-components/Email'
import Password from '../components/elemental-components/Password'

export function renderElement(element) {
  switch (element.type) {
    case enums.ELEMENT_TYPES.TEXT_BOX:
      return <Textbox element={element} />

    case enums.ELEMENT_TYPES.BUTTON:
      return <Button element={element} />

    case enums.ELEMENT_TYPES.TEXT_VIEW:
      return <Textview element={element} />

    case enums.ELEMENT_TYPES.SELECT:
      return <Selectbox element={element} />

    case enums.ELEMENT_TYPES.EMAIL:
      return <Email element={element} />

    case enums.ELEMENT_TYPES.PASSWORD:
      return <Password element={element} />
  }
}