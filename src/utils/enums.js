export const ELEMENT_TYPES = {
  TEXT_BOX: 'textbox',
  EMAIL: 'email',
  PASSWORD: 'password',
  BUTTON: 'button',
  TEXT_VIEW: 'textview',
  SELECT: 'select'
}