import React from 'react';
import './css/App.css';
import Draggable from './components/Draggable.js'
import Droppable from './components/Droppable.js'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isMounted: false
    }
  }

  componentDidMount() {
    this.setState({ isMounted: true })
  }

  saveState() {
    this.refs.droppable.saveState()
  }

   render() {
    return (
      <div className="App clearfix">
        <div className="row">
          <div className="col-3">
            <div className="bg-light border-right" >
              <div className="heading components-header pt-4">COMPONENTS</div>
              <div className="list-group list-group-flush">
                <Draggable />
              </div>
            </div>
          </div>
          <div className="col-9">
            <nav className="navbar navbar-expand-lg navbar-light bg-light border-bottom pr-5" id="app-header">
              <ul className="navbar-nav mt-2 mt-lg-0">
                <li className="nav-item heading">BOB THE BUILDER</li>
              </ul>
              <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
                <li className="nav-item ml-2">
                  <button className="btn btn-success" onClick={this.saveState.bind(this)}>Save</button>
                </li>
              </ul>
            </nav>
            <small className="font-italic font-weight-lighter float-right mr-4 text-secondary">Auto Save Enabled</small>
            <Droppable isMounted={this.state.isMounted} ref="droppable" />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
